import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  // LOADING SPINNER AND OTHER SERVICE FUNCTIONS LIKE SCROLL OR STUFF IN HERE PLEASE!
  isLoading = false;
  loading;
  constructor(public toastController: ToastController,
              public loadingController: LoadingController) {
   }

   async presentToast(message) {
     const toast = await this.toastController.create({
      message,
      duration: 2000
     });
     toast.present();
   }


   async presentLoading(message) {
      this.isLoading = true;
      this.loading = await this.loadingController.create({
       message
     }).then(res => {
        res.present().then(() => {
          if (!this.isLoading) {
            res.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
   }


  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingController
      .dismiss()
      .then(() => console.log('dismissed'));
  }
}

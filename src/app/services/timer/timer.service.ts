import { Injectable } from '@angular/core';
import { timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  
  source;
  constructor() { 
  	this.source = timer(1000, 1000);
  }
}

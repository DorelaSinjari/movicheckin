import { Injectable } from '@angular/core';
import { RequestsService } from '../../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  endpoint: string = Constant.existingCheckin;

  constructor(private requestsService: RequestsService) { }

  controllCheckin() {
    return this.requestsService.getRequest(this.endpoint);
  }
}

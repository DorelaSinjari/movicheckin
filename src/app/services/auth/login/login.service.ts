import { Injectable } from '@angular/core';
import { RequestsService } from '../../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userLogin: string = Constant.userLogin;
  userLoginImei: string = Constant.userLoginImei;
  deviceLogin: string = Constant.userLoginDevice;

  constructor(private requestsService: RequestsService) { }

  login(body) {
    return this.requestsService.postRequest(this.userLogin, body);
  }

  confirmImei(body) {
    return this.requestsService.postRequest(this.userLoginImei, body);
  }

  loginDevice(body) {
    return this.requestsService.postRequest(this.deviceLogin, body);
  }
}

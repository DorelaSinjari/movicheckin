import { TestBed } from '@angular/core/testing';

import { EntranceService } from './entrance.service';

describe('EntranceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EntranceService = TestBed.get(EntranceService);
    expect(service).toBeTruthy();
  });
});

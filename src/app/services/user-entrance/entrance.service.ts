import { Injectable } from '@angular/core';
import { RequestsService } from '../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class EntranceService {

  endpoint: string = Constant.checkin;
  causaliEndpoint: string = Constant.getCausali;

  constructor(private requestService: RequestsService) { }

  registerEntrance(body) {
    return this.requestService.postRequest(this.endpoint, body);
  }

  getCausalsRequest() {
    return this.requestService.getRequest(this.causaliEndpoint);
  }
}

import { Injectable } from '@angular/core';
import { RequestsService } from '../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class GeofenceService {
  getpositions: string = Constant.positions;
  verify_positon: string = Constant.verifyPosition;

  constructor(private requestService: RequestsService) { }

    getPositions() {
      return this.requestService.getRequest(this.getpositions);
    }

    postVerifyPosition(body) {
      return this.requestService.postRequest(this.verify_positon, body);
    }
}

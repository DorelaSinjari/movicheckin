import { TestBed } from '@angular/core/testing';

import { ExitService } from './exit.service';

describe('ExitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExitService = TestBed.get(ExitService);
    expect(service).toBeTruthy();
  });
});

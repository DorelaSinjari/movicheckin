import { Injectable } from '@angular/core';
import { RequestsService } from '../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class ExitService {

  endpoint: string  = Constant.checkout;
  constructor(private requestService: RequestsService) { }

  registerExit(body) {
    return this.requestService.postRequest(this.endpoint, body);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  private url: string = Constant.url;

  constructor(private http: HttpClient) { }

  getRequest(endpoint, params?) {
    return this.http.get(this.url + endpoint, {params});
  }

  postRequest(endpoint, body) {
    return this.http.post(this.url + endpoint, body);
  }

  putRequest(endpoint, id, body) {
    return this.http.put(this.url + endpoint, id, body);
  }

  deleteRequest(endpoint, id) {
    return this.http.delete(this.url + endpoint, id);
  }
}

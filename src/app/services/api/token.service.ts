import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  token;

  constructor() {
    this.token = "";
  }

  setToken(val: string) {
    this.token = val;
  }

  getToken() {
    return this.token;
  }

  removeToken() {
    this.token = null;
  }
}

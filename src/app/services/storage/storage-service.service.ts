import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {

  constructor(private storage: Storage) { }

  get(key: string) {
    return this.storage.get(key);
  }

  set(key: string, value: string) {
    return this.storage.set(key, value);
  }

  remove(key: string) {
    return this.storage.remove(key);
  }
}

import { Injectable } from '@angular/core';
import { RequestsService } from '../api/requests/requests.service';
import { Constant } from 'src/app/helpers/constant/constant';

@Injectable({
  providedIn: 'root'
})
export class StampingsService {

  endpoint: string = Constant.getLogs;
  constructor(private requestService: RequestsService) { }

  getStampingsHistory() {
    return this.requestService.getRequest(this.endpoint);
  }
}

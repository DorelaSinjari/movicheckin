import { TestBed } from '@angular/core/testing';

import { StampingsService } from './stampings.service';

describe('StampingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StampingsService  = TestBed.get(StampingsService );
    expect(service).toBeTruthy();
  });
});

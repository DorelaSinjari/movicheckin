import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageServiceService } from '../services/storage/storage-service.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(private storageService: StorageServiceService,
              private navCtrl: NavController) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> | boolean {
     return new Promise( (resolve) => {
      this.storageService.get('companyCode')
        .then((value) => {
          if (value) {
            resolve(true);
          } else {
            this.navCtrl.navigateForward(['login']);
            resolve(false);
          }
        })
        .catch((error) => {
          this.navCtrl.navigateBack(['login']);
        });
  });
  }
}

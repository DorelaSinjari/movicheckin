import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';
import { GeofenceService } from 'src/app/services/geofence/geofence.service';
import { Position } from 'src/app/interfaces/position';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss'],
})
export class PositionComponent implements OnInit {

  selectedPosition : number;
  positionArray : Position[];
  searchTerm: string;
  pause : boolean = false;
  positionsArray: Position[] = [];
  filterpositionsArray: Position[] = [];
  emptyCase : boolean = false;
  constructor(public popoverController: PopoverController,
              public navParams: NavParams,
              private geofenceService: GeofenceService) {}

  ngOnInit() {
    this.proponiPosizione();
  }

  proponiPosizione() {
    this.geofenceService.getPositions().subscribe((response : Position[]) => {
      if (!!response) {
        this.positionsArray = response;
        this.pause = true;
        this.emptyCase =  false;
      }
    });
  }

  filterSearch(term) {
    this.filterpositionsArray = [];
    this.searchTerm = term.target.value;
    if (this.searchTerm.length !== 0) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.positionsArray.length; i++) {
        if (this.positionsArray[i].desPosizione.toLowerCase().includes(this.searchTerm.toLowerCase())) {
          this.filterpositionsArray.push(this.positionsArray[i]);
          this.emptyCase = false;
        }
      }
    } else if (this.searchTerm === '' || this.searchTerm === null) {
       this.filterpositionsArray = [];
       this.proponiPosizione();
    }
  }

  async select(value) {
    this.selectedPosition = value;
  }

  selectedValue() {
    if (!!this.selectedPosition || this.selectedPosition === 0) {
      this.cancel(this.selectedPosition);
    }
  }

  cancel(value?) {
    this.popoverController.dismiss(value);
    // this.popoverController.getTop().then((response) => {
    //   response.dismiss(value);
    // });
  }


}

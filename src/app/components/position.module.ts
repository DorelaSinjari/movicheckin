import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionComponent } from './position/position.component';



@NgModule({
  declarations: [PositionComponent],
  imports: [
    CommonModule
  ],
  exports: [PositionComponent],
  entryComponents: [PositionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PositionModule { }

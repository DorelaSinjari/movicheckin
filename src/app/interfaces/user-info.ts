export interface UserInfo {
	iD_DipMov: number,
	iD_Dipend: number,
	timeIn: string,
	timeOut: string,
	note: string,
	gpSin: string, 
	gpSout: string, 
	imeIin: string, 
	imeIout: string, 
	timeINSin: string, 
	timeINSout: string,
	timeMod: string,
	causale: object,
	posizione_In: string,
	posizione_Out: string,
	quantita: number
}
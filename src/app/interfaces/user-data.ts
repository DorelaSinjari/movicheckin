export interface UserData {
	imei:string,
	userId:number,
	token:string,
	username:string,
	companyCode:string,
	firstName:string,
	lastName:string
}

export interface Causale {
	id_Cau: number,
	codCau: string,
	desCau: string,
	qtaReq: boolean,
	id_Posizione: number
}

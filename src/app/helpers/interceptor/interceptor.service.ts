import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../../../app/services/api/token.service';
import { NavController } from '@ionic/angular';
import { StorageServiceService } from '../../services/storage/storage-service.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class InterceptorService  implements HttpInterceptor {

  constructor( private storageService: StorageServiceService,
               private tokenService: TokenService,
               private navCtrl: NavController ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler): Observable<HttpEvent<any>> {

      const token = this.tokenService.getToken();

      if (token) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
            'X-Requested-With': 'XmlHttpRequest',
            Accept: 'application/json',
            'Content-Type': 'application/json',
            processData: 'false'
          }
        });
      } else {
        req = req.clone({
          setHeaders: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XmlHttpRequest',
            Accept: 'application/json'
          }
        });
       }
      return next.handle(req).pipe(
        tap(
          event => {
            // console.log(event);
          },
          error => {
            if (error.status === 401) {
              this.storageService.remove('companyCode').then((res)=>{
                this.navCtrl.navigateForward(['/login']);
                this.tokenService.setToken(null);
              });
            }
          }
        ),
      );
  }
}

export class Constant {

    public static base_url = 'https://movicheckin-webapi-qc.azurewebsites.net';
    public static url = 'https://movicheckin-geofencing-api-qc.azurewebsites.net/api/';

    // User Login Constants
    public static userLogin = 'User/VerifyCredentials';
    public static userLoginImei = 'User/ConfirmIMEI';
    public static userLoginDevice = 'User/DeviceLogin';

    // TimeLog Constants
    public static existingCheckin = 'TimeLog/GetExistingCheckin';
    public static checkin = 'TimeLog/CheckIn';
    public static checkout = 'TimeLog/CheckOut';
    public static getLogs = 'TimeLog/GetAllLogs';
    public static getCausali = 'TimeLog/GetCausali';

    // Positions
    public static positions = 'Position/GetPositons';
    public static verifyPosition = 'Position/VerifyPosition';
}

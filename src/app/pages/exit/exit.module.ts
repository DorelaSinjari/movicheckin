import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExitPageRoutingModule } from './exit-routing.module';

import { ExitPage } from './exit.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { PositionModule } from 'src/app/components/position.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExitPageRoutingModule,
    HeaderModule,
    PositionModule
  ],
  declarations: [ExitPage]
})
export class ExitPageModule {}

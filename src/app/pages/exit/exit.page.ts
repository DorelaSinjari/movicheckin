import { Component, OnInit } from '@angular/core';
import { ExitService } from 'src/app/services/user-exit/exit.service';
import { UiService } from 'src/app/services/ui/ui.service';
import { NavController, Events, Platform, PopoverController, AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageServiceService } from 'src/app/services/storage/storage-service.service';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { PositionComponent } from 'src/app/components/position/position.component';
import { GeofenceService } from 'src/app/services/geofence/geofence.service';
import { TimerService } from 'src/app/services/timer/timer.service';
import { Causale } from 'src/app/interfaces/causale';
import { Position } from 'src/app/interfaces/position';


@Component({
  selector: 'app-exit',
  templateUrl: './exit.page.html',
  styleUrls: ['./exit.page.scss'],
})
export class ExitPage implements OnInit {

  leavedNote : string = 'Scrivi i tuoi appunti...';
  buttonName : string = 'Registra FINE';
  backButtonName : string  = 'Indietro';
  positionGPS: string = '';
  time : string;
  imei : string;
  geoLocation: string = '';
  causaliList: Causale[];
  quantita : string;
  descriptionCausale : string;
  codiceCausale : number;
  error:boolean = false;
  errorMessage : string = '';
  quantitaUser: number;
  // radiogroup;
  positionID : number;
  positionSelectedDes: string;
  positionsArray: Position[] = [];
  causalePosition : string = null;
  lat : number;
  lng : number;
  activate: boolean = true;
  saveGPSLocation: boolean = false;
  errorLocation: string = '';
  causaleSelected : number;
  causalePosizione : string;
  causalePosizioneId  : number;
  causaleObjectPosizione : Position;
  timeSubscriber;

  constructor(private exitService: ExitService,
              private uiService: UiService,
              public navCtrl: NavController,
              private diagnostic: Diagnostic,
              private openNativeSettings: OpenNativeSettings,
              public platform: Platform,
              public popoverController: PopoverController,
              public events: Events,
              public alertController: AlertController,
              private storageService: StorageServiceService,
              private geofenceService: GeofenceService,
              private geolocation: Geolocation,
              private timeService : TimerService) {
  }

  /**
  *  When initialized the exit page get necessary informations like: 
  *  User Location, Causale List, Position proposed,storage and initialise timer.
  **/
  async ngOnInit() {
    this.initTimer();
    this.getLocation();
    await this.storageFunction();
    await this.getNote();  
  }

  /**
  *  Subscribe for timer every second change date & time.
  **/
  initTimer(){
    this.timeSubscriber = this.timeService.source.subscribe(val => this.getTime());
  }
  

  async getNote() {
    this.leavedNote = await this.storageService.get('Note');
    if (!this.leavedNote) {
      this.leavedNote = ' ';
    }
  }

  /**
  *  Get informations saved in storage like imei and casuale selected during checkin
  **/
  async storageFunction() {
    this.imei = await this.storageService.get('imei');
    if (!this.imei) {
      this.uiService.presentToast('C\'è stato un errore, prego inserire le giuste credenziali');
      //Unsubscribe to timer observer
      this.timeSubscriber.unsubscribe();
      this.navCtrl.navigateRoot('login');
    }
    this.codiceCausale = await this.storageService.get('codCau');
    this.descriptionCausale = await this.storageService.get('desCau');
    this.quantita = await this.storageService.get('QtaReq');
    this.causaleSelected = await this.storageService.get('CauSelected');
    if (!!this.causaleSelected) {
      this.causalePosition = await this.storageService.get('desPosizione');
      this.causalePosizioneId = await this.storageService.get('id_Posizione');
      this.causaleObjectPosizione = { id_Posizione: this.causalePosizioneId, desPosizione: this.causalePosition};
    }
  }

  /**
  *  Get time to show to user.
  **/
  getTime() {
    let currentDate: any =  new Date().toLocaleString('it-IT', {timeZone: 'Europe/Rome'});
    currentDate = currentDate.split(',');
    this.time = currentDate[1];
  }


  /**
  *  Start the exit functionality when button pressed
  **/
  exit() {
   if (!!this.positionID || this.positionID === 0) {
      if (!this.quantita) {
        let body = {
          Note: this.leavedNote,
          GPS: this.geoLocation,
          IMEI: this.imei,
          Quantita: 0,
          Id_PosizioneOUT: this.positionID
        };
        this.error = false;
        this.errorMessage = '';
        this.exitRequest(body);
      } else  if (!!this.quantitaUser) {
        let body = {
          Note: this.leavedNote,
          GPS: this.geoLocation,
          IMEI: this.imei,
          Quantita: this.quantitaUser,
          Id_PosizioneOUT: this.positionID
        };
        this.error = false;
        this.errorMessage = '';
        this.exitRequest(body);
      } else if (!this.quantitaUser) {
        this.error = true;
        this.errorMessage = 'Prego inserire la quantità';
        return;
      }
   } else if (this.leavedNote.length !== 0 && !!this.saveGPSLocation && !this.positionID) {
    if (!this.quantita) {
      let body = {
        Note: this.leavedNote,
        GPS: this.geoLocation,
        IMEI: this.imei,
        Quantita: 0,
        Id_PosizioneOUT: null
      };
      this.error = false;
      this.errorMessage = '';
      this.exitRequest(body);
    } else  if (!!this.quantitaUser) {
      let body = {
        Note: this.leavedNote,
        GPS: this.geoLocation,
        IMEI: this.imei,
        Quantita: this.quantitaUser,
        Id_PosizioneOUT: null
      };
      this.error = false;
      this.errorMessage = '';
      this.exitRequest(body);
    } else if (!this.quantitaUser) {
      this.error = true;
      this.errorMessage = 'Prego inserire la quantità';
      return;
    }
   }
  }

  /**
  *  Exit request function, to send the exit request to api.
  **/
  exitRequest(body) {
    this.exitService.registerExit(body).subscribe( (response) => {
      this.uiService.presentLoading('Stiamo verificando, per favore attendi...');
      if (response) {
          this.storageService.remove('Note');
          this.events.publish('update Checkin History');
          this.events.publish('update checkin');
          //Unsubscribe to timer observer
          this.timeSubscriber.unsubscribe();
          this.navCtrl.navigateBack(['/home']);
          this.uiService.dismissLoading();
          this.uiService.presentToast('Uscita effettuata con successo!');
        }
      }, error => {
          this.uiService.presentToast('Selezionare un valore nel campo Posizione');
      });
  }

  /**
  *  go back functionality on back button clicket by user.
  **/
  backButton() {
    this.uiService.presentLoading('Per favore attendi...');
    //Unsubscribe to timer observer
    this.timeSubscriber.unsubscribe();
    this.navCtrl.navigateBack(['/home']);
    this.uiService.dismissLoading();
  }

  /**
  *  Get user location 
  **/
  async getLocation() {
    if (this.platform.is('ios')) {
      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
        if (response) {
          const position = response;
          this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          return this.setEntrancePos();
        }
      }).catch((error) => {
        this.uiService.presentToast('La tua posizione non è stata trovata.');
      });
    } else {
      this.diagnostic.isGpsLocationAvailable().then((available: any) => {
        if (!available) {
            this.openNativeSettings.open('location').then((value) => {
              this.diagnostic.isGpsLocationAvailable().then((available: any) => {
                this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
                if (response) {
                  const position = response;
                  this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
                  this.lat = position.coords.latitude;
                  this.lng = position.coords.longitude;
                  return this.setEntrancePos();
                }
              }).catch((error) => {
                this.uiService.presentToast('La tua posizione non è stata trovata.');
              });
            });
          });
        } else if (available) {
          this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
            if (response) {
              const position = response;
              this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
              this.lat = position.coords.latitude;
              this.lng = position.coords.longitude;
              return this.setEntrancePos();
            }
          }).catch((error) => {
            this.uiService.presentToast('La tua posizione non è stata trovata.');
          });
        } else {
          this.uiService.presentToast('La tua posizione non è stata trovata.');
        }
      });
    }
  }

  async setEntrancePos(){
    this.positionID = await this.storageService.get('id_Posizione');
    this.positionSelectedDes = await this.storageService.get('desPosizione');
    this.proponiPosizione();
  }

  /**
  *  Get the array of positions from api.
  **/   
  proponiPosizione() {
    this.geofenceService.getPositions().subscribe((response: Position[]) => {
      if (!!response) {
        this.positionsArray = response;
        // let position = this.positionsArray.find(el => !!el.tipoPosizione && el.tipoPosizione == 'Posizione_Utente');
        // if(!!position){
        //   this.positionID = position.id_Posizione;
        //   this.positionSelectedDes = position.desPosizione;
        // }else{
        //   let position = this.positionsArray.find(el => !!el.tipoPosizione && el.tipoPosizione == 'Posizione_Azienda');
        //   if(!!position){
        //     this.positionID = position.id_Posizione;
        //     this.positionSelectedDes = position.desPosizione;
        //   }
        // }
      } 
    });
  }

  async positionPospover() {
    const popover = await this.popoverController.create({
      component: PositionComponent,
      backdropDismiss: false,
      componentProps: { causalePosizione: this.causaleObjectPosizione }
    });
    popover.onDidDismiss().then((response) => {
      this.positionID = response.data;
      console.log(this.positionID);
      if (this.positionsArray.length > 0 && (!!this.positionID || this.positionID === 0)) {
        let item = this.positionsArray.find((element: any) => element.id_Posizione === this.positionID);
        console.log(item);
        this.positionSelectedDes = item.desPosizione;
      } 
    });
    return await popover.present();
  }

  verificaPozicione(idPosition, lat, lng) {
    let body;
  
    if ((!!idPosition || idPosition === 0)) {
      body = {
        lat: lat,
        lng: lng,
        positionId: idPosition
      };
    }
    this.geofenceService.postVerifyPosition(body).subscribe((response) => {
      if (!response) {
        this.activate = false;
        this.positionID = null;
        this.alertGPSPermision();
      } else if (!!response) {
        this.activate = true;
      }
    });
  }

  async alertGPSPermision() {
      const alert = await this.alertController.create({
        subHeader: `Il dispositivo non risulta nel perimetro della posizione dichiarata.
                    Riprovare oppure specificare la causa nel campo note.
                    Acconsenti al rilevamento della posizione GPS?`,
        buttons: [
        {
          text: 'Cancella',
          role: 'cancel',
          handler: () => {
                  this.backButton();
                }
        },
        {
        text: 'Accetta',
        handler: () => {
                  this.saveGPSLocation = true;
                }
        }],
        backdropDismiss: false
      });
      return await alert.present();
    }

    leaveNote(note) {
      if (this.leavedNote) {
        this.activate = true;
      } else { this.activate = false; }
    }

    givenPosition(event: any) {
      this.errorLocation = '';
      let item;
      if (this.positionsArray.length > 0) {
        item = this.positionsArray.find( (element: any) =>
        element.desPosizione.toLowerCase() === event.target.value.toLowerCase() );
        if (!!item) {
          this.positionID = item.id_Posizione;
          this.verificaPozicione(this.positionID, this.lat, this.lng);
        } else if (!item) {
          this.errorLocation = 'La tua posizione non è stata trovata nella distanza richiesta!';
        }
      } else { this.backButton(); }
    }
}

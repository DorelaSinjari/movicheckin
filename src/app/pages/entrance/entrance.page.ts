import { Component, OnInit, ɵɵresolveBody } from '@angular/core';
import { EntranceService } from 'src/app/services/user-entrance/entrance.service';
import { UiService } from 'src/app/services/ui/ui.service';
import { NavController, Events, AlertController, Platform, PopoverController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { StorageServiceService } from 'src/app/services/storage/storage-service.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { PositionComponent } from 'src/app/components/position/position.component';
import { GeofenceService } from 'src/app/services/geofence/geofence.service';
import { logging } from 'protractor';
import { TimerService } from 'src/app/services/timer/timer.service';
import { Causale } from 'src/app/interfaces/causale';
import { Position } from 'src/app/interfaces/position';


@Component({
  selector: 'app-entrance',
  templateUrl: './entrance.page.html',
  styleUrls: ['./entrance.page.scss'],
})

export class EntrancePage implements OnInit {

  leavedNote: string = '';
  buttonName: string = 'Registra INIZIO';
  backButtonName: string = 'Indietro';
  positionGPS: string = '';
  time:string;
  imei: string;
  geoLocation: string = '';
  errorMessage: string = '';
  error: boolean = false;
  causaliList: Causale[];
  causaleDescription: string = '';
  getCausaleCode: string;
  writtedByClientCode: number;
  barcodeClientCode: string;
  selectedClientCode: string;
  // radiogroup;
  positionID:number;
  positionSelectedDes: string;
  positionsArray: Position[] = [];
  causalePosition : number = null;
  lat : number;
  lng : number;
  activate: boolean = true;
  saveGPSLocation: boolean = false;
  errorPositon : string = '';
  timeSubscriber;


  constructor(private entranceService: EntranceService,
              private uiService: UiService,
              private diagnostic: Diagnostic,
              private openNativeSettings: OpenNativeSettings,
              public navCtrl: NavController,
              private storageService: StorageServiceService,
              public events: Events,
              public platform: Platform,
              public popoverController: PopoverController,
              public alertController: AlertController,
              private barcodeScanner: BarcodeScanner,
              private geofenceService: GeofenceService,
              private geolocation: Geolocation,
              private timeService : TimerService) {
  }


  /**
  *  When initialized the entrance get necessary informations like: 
  *  User Location, Causale List, Position proposed,storage and initialise timer.
  **/
  async ngOnInit() {
    this.getLocation();
    await this.storageFunction();
    this.getCausaliList();
    this.initTimer();
  }

  /**
  *  Subscribe to timer observer every second change date & time.
  **/
  initTimer(){
    this.timeSubscriber = this.timeService.source.subscribe(val => this.getTime());
  }
  

  /**
  *  Get Imei information saved in storage
  **/
  async storageFunction() {
    this.imei = await this.storageService.get('imei');
    if (!this.imei) {
      this.uiService.presentToast('C\'è stato un errore, prego inserire le giuste credenziali');
      //Unsubscribe to timer observer
      this.timeSubscriber.unsubscribe();
      this.navCtrl.navigateForward('login');
    }
  }

  /**
  *  Get time to show to user.
  **/
  getTime() {
    let currentDate =  new Date().toLocaleString('it-IT', {timeZone: 'Europe/Rome'});
    this.time = currentDate.split(',')[1];
  }

  /**
  *  On change of causale get the casuale description to show on frontend.
  *  On change of causale check also if causale has proposed location and
  *  verify if user is near the causale position.
  **/
  displayDescription(term) {

    if (this.causaliList.length !== 0 && !!this.causaliList) {

      let causaleSelected: any;
      let listCausali: Causale[] = this.causaliList;
      causaleSelected = listCausali.find((el: any) => el.codCau === term);

      //if you are using valid causale.
      if (!!causaleSelected) {

        this.causaleDescription = causaleSelected.desCau;
        this.writtedByClientCode = causaleSelected.id_Cau;
        this.causalePosition = causaleSelected.id_Posizione;

        if (!!this.causalePosition && this.causalePosition === 0) {
          this.positionID = this.causalePosition;
          // this.verificaPozicione(this.causalePosition, this.lat, this.lng);
        }

        this.errorMessage = '';
        this.error = false;

      //If you are not using causale.
      } else if (term === '' || term == null) {

        this.causaleDescription = '';
        this.writtedByClientCode = null;
        this.causalePosition = null;
        this.positionID = null;
        this.errorMessage = '';
        this.error = false;

      //If the code is not in the list.
      } else {

         this.causaleDescription = '';
         this.writtedByClientCode = null;
         this.causalePosition = null;
         this.positionID = null;
         this.errorMessage = 'Codice causale non esistente!';
         this.error = true;
      
      }
    }
  }

  /**
  *  Register Entrance function
  **/
  async entranceRegistered() {
    console.log(this.positionID);
    //Constructing body depending on causale and position
    let body;
    if (!!this.positionID || this.positionID === 0) {
      if (!!this.writtedByClientCode) {
        body = {
          Note: this.leavedNote,
          GPS: this.geoLocation,
          IMEI: this.imei,
          IdCausale: this.writtedByClientCode,
          Id_PosizioneIN: this.positionID
        };
      } else {
        body = {
          Note: this.leavedNote,
          GPS: this.geoLocation,
          IMEI: this.imei,
          IdCausale: null,
          Id_PosizioneIN: this.positionID
        };
      }
    } else if (this.leavedNote.length !== 0 && !!this.saveGPSLocation && !this.positionID) {
      if (!!this.writtedByClientCode) {
        body = {
            Note: this.leavedNote,
            GPS: this.geoLocation,
            IMEI: this.imei,
            IdCausale: this.writtedByClientCode,
            Id_PosizioneIN: null
        };
      } else {
        body = {
          Note: this.leavedNote,
          GPS: this.geoLocation,
          IMEI: this.imei,
          IdCausale: null,
          Id_PosizioneIN: null
        };
      }
    }

    //Send request to api
    this.entranceService.registerEntrance(body).subscribe( (response) => {
      this.uiService.presentLoading('Stiamo verificando, per favore attendi...');
      //if all ok, update home page and return back to home page
      if (response) {
        this.events.publish('update Checkin History');
        this.events.publish('update checkin');
        //Unsubscribe to timer observer
        this.timeSubscriber.unsubscribe();
        this.navCtrl.navigateBack(['/home']);
        this.uiService.dismissLoading();
        this.uiService.presentToast('Entrata effettuata con successo!');
        }
      }, error => {
        //if any error in the api show error message
        this.uiService.presentToast('Selezionare un valore nel campo Posizione');
      });
  }

  /**
  *  go back functionality on back button clicket by user.
  **/
  backButton() {
    this.uiService.presentLoading('Per favore attendi...');
    //Unsubscribe to timer observer
    this.timeSubscriber.unsubscribe();
    this.navCtrl.navigateBack(['/home']);
    this.uiService.dismissLoading();
  }


  /**
  *  Get user location 
  **/
  getLocation() {
    //if platform is ios get directly the position since ios handles the permissions
    if (this.platform.is('ios')) {
      this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
        if (response) {
          const position = response;
          this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
          this.lat =  position.coords.latitude;
          this.lng =  position.coords.longitude;
          this.proponiPosizione();
        }
      }).catch((error) => {
        this.uiService.presentToast('La tua posizione non è stata trovata.');
      });
    } else {
    //if platform is android check for permissions 
      this.diagnostic.isGpsLocationAvailable().then((available: any) => {
        if (!available) {
          //if no permission open settings for location
            this.openNativeSettings.open('location').then((value) => {
              //after closing location settings , did user enable permission
              this.diagnostic.isGpsLocationAvailable().then((available: any) => {
                //get location
                this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
                if (response) {
                  const position = response;
                  this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
                  this.lat =  position.coords.latitude;
                  this.lng =  position.coords.longitude;
                  this.proponiPosizione();
                }
              }).catch((error) => {
                //if any error in gps show error. 
                this.uiService.presentToast('La tua posizione non è stata trovata.');
              });
            });
          });
        } else if (available) {
          //if permission is available get user position. 
          this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then((response) => {
            if (response) {
              const position = response;
              this.geoLocation = position.coords.latitude + ', ' + position.coords.longitude;
              this.lat =  position.coords.latitude;
              this.lng =  position.coords.longitude;
              this.proponiPosizione();
            }
          }).catch((error) => {
                //if any error in gps show error. 
            this.uiService.presentToast('La tua posizione non è stata trovata.');
          });
        } else {
          //if any error in gps show error. 
          this.uiService.presentToast('La tua posizione non è stata trovata.');
        }
      });
    }
  }

  /**
  *  Get barcode information and get the casuale information from the code information
  **/ 
  getBarcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      let barCausaleCode = barcodeData.text;
      if (this.causaliList.length !== 0 && !!this.causaliList) {

        //Find the causale with the code equal to barcode
        let causale = this.causaliList.find((elm) => elm.codCau == barCausaleCode);

        //if we find the causale we utilize it
        if(!!causale){
          this.getCausaleCode = causale.codCau;
          this.causaleDescription = causale.desCau;
          this.writtedByClientCode = causale.id_Cau;
          this.causalePosition = causale.id_Posizione;
          // this.verificaPozicione(this.causalePosition, this.lat, this.lng);
          this.errorMessage = '';
          this.error = false;
        }else{
          //if we don't find the causale we show error.
         this.error = true;
         this.errorMessage = 'Il barcode inserito non è un codice causale esistente!';
         this.getCausaleCode = barCausaleCode;
         this.causalePosition = null;
         this.positionID = 0;
         this.causaleDescription = '';
         this.writtedByClientCode = null;
        }
      } else { 
        //if there are no causale show error.
        this.uiService.presentToast("Non c'i sono causali."); 
      }
    }).catch(err => {
        //if there is an error with the barcode show error.
        this.uiService.presentToast('Il barcode non è stato trovato con successo.');
    });
  }

  /**
  *  Get the list of causale.
  **/ 
  getCausaliList() {
    this.entranceService.getCausalsRequest().subscribe((listCausals: []) => {
      if (!!listCausals) {
        this.causaliList = listCausals;
      }
    });
  }

  /**
  *  Causale popover where you can select a causale from the list.
  **/ 
  async causaliPopover() {
   const options = {
     header: 'Seleziona una causale',
     inputs: [],
     buttons: [
      { text: 'cancella'},
      { text: 'seleziona',
        handler: (value) => {
          if(!!value){

            //Handle choosing the causale
              this.getCausaleCode = value.codCau;
              this.causaleDescription = value.desCau;
              this.causalePosition = value.id_Posizione;
              //if the causale has position verify it
              if (!!this.causalePosition || this.causalePosition === 0) {
                let position = this.positionsArray.find(el => el.id_Posizione == this.causalePosition);
                if(!!position){
                  this.positionID = position.id_Posizione;
                  this.positionSelectedDes = position.desPosizione;
                }
                // this.verificaPozicione(this.causalePosition, this.lat, this.lng);
              }
              this.writtedByClientCode = value.codCau;
              this.errorMessage = '';
              this.error = false;
            } else {
              
              this.getCausaleCode = null;
              this.causaleDescription = null;
              this.causalePosition = null;
              this.writtedByClientCode = null;
              this.errorMessage = '';
              this.error = false;
              
              
            }
          }
      }]
    };
    options.inputs.push({
      name: "Nessuna Causale",
      type: 'radio',
      value: null,
      label: "Nessuna Causale",
      // cssClass: 'alert-radio'
    });
   if (!!this.causaliList && this.causaliList.length > 0) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.causaliList.length; i++) {
        options.inputs.push({
          name: this.causaliList[i].id_Cau,
          type: 'radio',
          value: this.causaliList[i],
          label: `${this.causaliList[i].codCau}, ${this.causaliList[i].desCau}`,
          // cssClass: 'alert-radio'
        });
      }
    }

  //create alert and present it.
   const alert = await this.alertController.create(options);
   return await alert.present();
 }


  /**
  *  Get the array of positions from api.
  **/   
  proponiPosizione() {
    this.geofenceService.getPositions().subscribe((response:Position[]) => {
      if (!!response) {
        this.positionsArray = response;
        let position = this.positionsArray.find(el => !!el.tipoPosizione && el.tipoPosizione == 'Posizione_Utente');
        if(!!position){
          this.positionID = position.id_Posizione;
          this.positionSelectedDes = position.desPosizione;
        }else{
          let position = this.positionsArray.find(el => !!el.tipoPosizione && el.tipoPosizione == 'Posizione_Azienda');
          if(!!position){
            this.positionID = position.id_Posizione;
            this.positionSelectedDes = position.desPosizione;
          }
        }
      }
    });
  }

  /**
  *  Show list of positions and when popover closes get the selected position.
  **/  
  async positionPospover() {
    const popover = await this.popoverController.create({
      component: PositionComponent,
      backdropDismiss: false,
    });
    popover.onDidDismiss().then((response) => {
      this.positionID = response.data;
      if (this.positionsArray.length > 0 && (!!this.positionID || this.positionID === 0)) {
        let item = this.positionsArray.find((element: any) => element.id_Posizione === this.positionID);
        this.positionSelectedDes = item.desPosizione;
        // this.verificaPozicione(item.id_Posizione, this.lat, this.lng);
      }
    });
    return await popover.present();
  }

  /**
  *  Verify the position if its within geofence.
  **/  
  verificaPozicione(idPosition, lat, lng) {
    console.log(idPosition, lat, lng);
    let body;
    if ((!!idPosition || idPosition === 0) && !!lat && !!lng) {
      body = {
        lat: lat,
        lng: lng,
        positionId: idPosition
      };
    }
    this.geofenceService.postVerifyPosition(body).subscribe((response) => {
      if (!response) {
        this.activate = false;
        this.positionID = null;
        this.alertGPSPermision();
      } else if (!!response) {
        this.activate = true;
      }
    });
  }

  /**
  *  When the position of user is outside the geofence open alert.
  **/ 
  async alertGPSPermision() {
    const alert = await this.alertController.create({
      subHeader: `Il dispositivo non risulta nel perimetro della posizione dichiarata.
                  Riprovare oppure specificare la causa nel campo note.
                  Acconsenti al rilevamento della posizione GPS?`,
      buttons: [
      {
        text: 'Cancella',
        role: 'cancel',
        handler: () => {
              // this.backButton();
                  alert.dismiss();
              }
      },
      {
       text: 'Accetta',
       handler: () => {
                  alert.dismiss();
                  this.saveGPSLocation = true;
              }
      }],
      backdropDismiss: false
    });
    return await alert.present();
  }
  
  /**
  *  Set note informations and check if not empty space and activate button.
  **/
  leaveNote(note) {
    if (this.leavedNote && this.leavedNote !== ' ') {
      this.activate = true;
    } else { this.activate = false; }
  }
  
  /**
  *  When a position is selected verify position.
  **/
  givenPosition(event: any) {
    this.errorPositon = '';
    let item;
    if (this.positionsArray.length > 0) {
      item = this.positionsArray.filter( (element: any) =>
      element.desPosizione.toLowerCase() === event.target.value.toLowerCase() );
      if (!!item) {
        this.positionID = item[0].id_Posizione;
        this.verificaPozicione(this.positionID, this.lat, this.lng);
      } else if (!item) {
        this.errorPositon = 'La tua posizione non è stata trovata nella distanza richiesta!';
      }
    } else { this.backButton(); }
  }
}

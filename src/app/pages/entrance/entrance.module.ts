import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntrancePageRoutingModule } from './entrance-routing.module';

import { EntrancePage } from './entrance.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { PositionModule } from 'src/app/components/position.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntrancePageRoutingModule,
    HeaderModule,
    PositionModule
  ],
  declarations: [EntrancePage]
})
export class EntrancePageModule {}

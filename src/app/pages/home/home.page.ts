import { Component, OnInit, OnDestroy } from '@angular/core';
import { UiService } from 'src/app/services/ui/ui.service';
import { NavController, AlertController, Events } from '@ionic/angular';
import { LoginService } from 'src/app/services/auth/login/login.service';
import { StorageServiceService } from 'src/app/services/storage/storage-service.service';
import { HomeService } from 'src/app/services/auth/home/home.service';
import { TokenService } from 'src/app/services/api/token.service';
import { UserData } from 'src/app/interfaces/user-data';
import { TimerService } from 'src/app/services/timer/timer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit,OnDestroy {

  date: string;
  time: string;
  userData: UserData;
  checkinExists: boolean;
  noteLeaved: string = '';
  companyCode: string;
  imei: string;

  constructor(private uiService: UiService,
              public navCtrl: NavController,
              private loginService: LoginService,
              private storageService: StorageServiceService,
              public alertController: AlertController,
              private tokenService: TokenService,
              public events: Events,
              private homeService: HomeService,
              private timeService : TimerService) {}

  /**
  *  When initialized the home page subscribe to checkin event update & 
  *  Initialize the timer.
  **/
  async ngOnInit() {  
    this.events.subscribe('update checkin', () => {
      this.getIfCheckinExists();
    });
    this.initTimer();
  }


  /**
  *  Retrieve users informations from storage.
  **/
  async ionViewWillEnter() {
    await this.setCompanyName();
    this.getUserData();
  }
  
  /**
  *  Subscribe for timer every second change date & time.
  **/
  initTimer(){
    this.timeService.source.subscribe(val => this.getTime());
  }
  
  /**
  *  Unsubscribe to timer observer.
  **/
  ngOnDestroy(){
    this.timeService.source.unsubscribe();
  }


  /**
  *  Change date & time visualy and the variables to send to api.
  **/
  getTime() {
    //Utilizing the timezone of italy which gives a format dd/mm/yyyy , H:i:s
    let currentDate: any =  new Date().toLocaleString('it-IT', {timeZone: 'Europe/Rome'});
    currentDate = currentDate.split(',');
    this.time = currentDate[1];

    let date = currentDate[0];
    date = date.split('/');

    //Adding 0 on single numbers to make it look good
    let day = date[0];
    if (day.length === 1) {
      day = '0' + day;
    }

    //Adding 0 on single numbers to make it look good
    let month = date [1];
    if (month.length === 1) {
      month = '0' + month;
    }

    this.date = day + '/' + month + '/' + date[2];
  }

  /**
  *  Retrieve company and imei from storage.
  **/
  async setCompanyName() {
    this.companyCode = await this.storageService.get('companyCode');
    this.imei = await this.storageService.get('imei');
  }

  /**
  *  Loading during navigations.
  **/
  navigateStampingPage() {
    this.uiService.presentLoading('Per favore attendi...');
    this.navCtrl.navigateForward(['/stampings']);
    this.uiService.dismissLoading();
  }

  /**
  *  Loading during navigations.
  **/
  navigateEntrancePage() {
    this.uiService.presentLoading('Per favore attendi...');
    this.navCtrl.navigateForward(['/entrance']);
    this.uiService.dismissLoading();
  }

  /**
  *  Loading during navigations.
  **/
  navigateExitPage() {
    this.uiService.presentLoading('Per favore attendi...');
    this.navCtrl.navigateForward(['/exit']);
    this.uiService.dismissLoading();
  }

  /**
  *  Get User data from api.
  **/
  getUserData() {
    const body = {
      IMEI: this.imei,
      CompanyCode: this.companyCode
    };

    this.loginService.loginDevice(body).subscribe((response:UserData) => {
      this.userData = response;
      this.tokenService.setToken(this.userData.token);
      this.getIfCheckinExists();
    }, error => {
      console.log(error);
    });
  }

  /**
  *  Check if a checkin already exists so we can enable the checkout
  *  And disable the checkin. We remove previous information from storage
  *  And replace them with the new checkin information from server 
  **/
  getIfCheckinExists() {
    let userInfo: any;
    this.storageService.remove('QtaReq');
    this.storageService.remove('codCau');
    this.storageService.remove('desCau');
    this.storageService.remove('CauSelected');
    this.storageService.remove('desPosizione');
    this.storageService.remove('id_Posizione');

    this.homeService.controllCheckin().subscribe((response) => {
      if (response) {
        userInfo = response;
        this.noteLeaved = userInfo.note;
        this.storageService.set('Note', this.noteLeaved);
        this.checkinExists = userInfo.checkInExists;
        if (!!userInfo.causale) {
          this.storageService.set('QtaReq', userInfo.causale.qtaReq);
          this.storageService.set('codCau', userInfo.causale.codCau);
          this.storageService.set('desCau', userInfo.causale.desCau);
          if(!!userInfo.causale.posizione){
            this.storageService.set('desPosizione', userInfo.causale.posizione.desPosizione);
            this.storageService.set('id_Posizione', userInfo.causale.posizione.id_Posizione);
          }
        }
        if (!!userInfo.posizioneIn) {
          this.storageService.set('CauSelected', 'true');
          this.storageService.set('desPosizione', userInfo.posizioneIn.desPosizione);
          this.storageService.set('id_Posizione', userInfo.posizioneIn.id_Posizione);
        } else {
          this.storageService.set('CauSelected', 'false');
        }
      }
    });
  }

}

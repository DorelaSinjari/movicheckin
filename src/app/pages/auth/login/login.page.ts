import { Component, OnInit, ErrorHandler } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from 'src/app/services/ui/ui.service';
import { LoginService } from 'src/app/services/auth/login/login.service';
import { NavController, Platform, AlertController, PopoverController, LoadingController } from '@ionic/angular';
import { Uid } from '@ionic-native/uid/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { StorageServiceService } from 'src/app/services/storage/storage-service.service';
import { TokenService } from 'src/app/services/api/token.service';
import { UserData } from 'src/app/interfaces/user-data';
import { AppVersion } from '@ionic-native/app-version/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginForm: FormGroup;
  imeiDevice: string;
  userData : UserData;
  button: boolean = true;
  submitted: boolean = false;
  load : any;
  versionNumber : string ="";

  constructor(public formBuilder: FormBuilder,
              private uiService: UiService,
              public loadingController: LoadingController,
              private loginService: LoginService,
              public navCtrl: NavController,
              public platform: Platform,
              private uid: Uid,
              private uniqueDeviceID: UniqueDeviceID,
              private storageService: StorageServiceService,
              public alertController: AlertController,
              public popoverController: PopoverController,
              private tokenService: TokenService,
              private appVersion: AppVersion,
              private androidPermissions: AndroidPermissions) {

   }

  /**
  *  When initialized the login page retrieve imei & and start the formbuilder
  **/
  async ionViewWillEnter() {
      this.imeiDevice = await this.getUuid();
      this.versionNumber = await this.appVersion.getVersionNumber();
  }

  ngOnInit() { 
      this.loginForm = this.formBuilder.group({
        codiceAzienda: ['TestCompany', Validators.required],
        codiceUtente: ['test_user2', Validators.required],
        password: ['Admin123', Validators.required]
      });
  }

  /**
  *  When trying to login check the validation of the form and then send request
  **/
  login() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.button = true;
    } else {
      this.button = false;
      const body = {
        CompanyCode: this.loginForm.value.codiceAzienda,
        Username: this.loginForm.value.codiceUtente,
        Password: this.loginForm.value.password,
      };
      this.loginRequest(body);
    }
  }

  get validate() {
    return this.loginForm.controls;
  }

  /**
  *  Request for logging to the app.
  **/
  loginRequest(bodyData) {
    this.loading();
    this.loginService.login(bodyData).subscribe( (response: any) => {
      this.dismiss();
      this.userData = response;
      this.controllerImei();
     }, error => {
      this.load.dismiss();
      this.uiService.presentToast('Le tue credenziali sono errate.');
      this.button = true;
    });
  }


  /**
  *  function to retrieve imei.
  **/
  async getUuid() {

    if(this.platform.is('android')){

      const { hasPermission } = await this.androidPermissions.checkPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!hasPermission) {
       const result = await this.androidPermissions.requestPermission(
         this.androidPermissions.PERMISSION.READ_PHONE_STATE
       );

       if (!result.hasPermission) {
         throw new Error('Permissions required');
         return;
       }
      }
    }
  
    return this.imeiDevice = await this.uniqueDeviceID.get();
  }

  /**
  *  Check if the the user is utilizing a new device or not.
  *  If utilizing new device try to update device information with user.
  **/
  controllerImei() {
    //If the imei not retrieved open anomaly alert.
    if (this.imeiDevice === undefined) {
      this.createAlert();
      this.button = true;
    } else if (this.userData.imei === null || this.userData.imei === '' || this.userData.imei !== this.imeiDevice) {
      let body = {
        UserId: this.userData.userId,
        IMEI: this.imeiDevice
      };
      this.loginService.confirmImei(body).subscribe( (response) => {
        this.loginDevicePopUp();
        this.storageService.set('imei', this.imeiDevice);
        this.tokenService.setToken(this.userData.token);
      }, error => {
        this.uiService.presentToast('');
        this.button = true;
      });
    } else {
      this.loginDevicePopUp();
      this.storageService.set('imei', this.userData.imei);
    }
  }

  /**
  *  function to open alert for the anomaly information.
  **/
  async createAlert() {
    const alert = await this.alertController.create({
      header: 'ANOMALIA',
      message: 'Codice IMEI non rilevabile da questo dispositivo, la procedura di identificazione utente non può essere conclusa!',
      buttons: [{
        text: 'chiudi',
        role: 'cancel',
        handler: () => {
          alert.dismiss();
        }
      }]
    });
    await alert.present();
  }


  /**
  *  Function which opens the procedure of combining the new device imei
  *  and the user account.
  **/
  async loginDevicePopUp() {
    const alert = await this.alertController.create({
      message: 'Procedo con l’abbinamento di questo dispositivo all’anagrafica di ' + this.userData.username + ' ?',
      buttons: [{
        text: 'chiudi',
        role: 'cancel',
        handler: () => {
          this.storageService.remove('imei');
          this.tokenService.removeToken();
          this.navCtrl.navigateForward(['login']);
          this.button = true;
        }
      },
    {
      text: 'procedere',
      handler: () => {
        this.uiService.presentLoading('Stiamo verificando, per favore attendi...');
        this.storageService.set('companyCode', this.userData.companyCode);
        this.uiService.dismissLoading();
        this.navCtrl.navigateForward(['home']);
        this.button = true;
      }
    }]
    });
    await alert.present();
  }

  /**
  *  Open Loading.
  **/
  async loading() {
    this.load = await this.loadingController.create({message: 'Stiamo verificando, per favore attendi...'});
    this.load.present();
  }

  /**
  *  Dissmiss Loading
  **/
  async dismiss() {
    return await this.load.dismiss();
  }
}

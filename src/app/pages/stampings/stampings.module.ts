import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StampingsPageRoutingModule } from './stampings-routing.module';

import { StampingsPage } from './stampings.page';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StampingsPageRoutingModule,
    HeaderModule
  ],
  declarations: [StampingsPage]
})
export class StampingsPageModule {}

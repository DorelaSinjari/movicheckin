import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StampingsPage } from './stampings.page';

const routes: Routes = [
  {
    path: '',
    component: StampingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StampingsPageRoutingModule {}

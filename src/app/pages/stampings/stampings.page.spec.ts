import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StampingsPage } from './stampings.page';

describe('StampingsPage', () => {
  let component: StampingsPage;
  let fixture: ComponentFixture<StampingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StampingsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StampingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { StampingsService } from 'src/app/services/stampings/stampings.service';
import { EntranceService } from 'src/app/services/user-entrance/entrance.service';
import { UiService } from 'src/app/services/ui/ui.service';
import { NavController, AlertController, Events } from '@ionic/angular';
import { element } from 'protractor';
import { UserInfo } from 'src/app/interfaces/user-info';

@Component({
  selector: 'app-stampings',
  templateUrl: './stampings.page.html',
  styleUrls: ['./stampings.page.scss'],
})
export class StampingsPage implements OnInit {

  pageTitle : string = 'Storico timbrature';
  enteryDate : string = 'Data ingresso';
  exitDate : string = 'Data uscita';
  enteryTime : string = 'Ora ingresso';
  exitTime : string = 'Ora uscita';
  note : string = 'Nota';
  vedi : string = 'nota';
  backButtonName : string = 'Indietro';
  userInfo: UserInfo[];
  error : string;
  dataComming : boolean = false;

  constructor(private stampingsService: StampingsService,
              private entranceService: EntranceService,
              private uiService: UiService,
              public navCtrl: NavController,
              public events: Events,
              public alertController: AlertController) {
  }

  /**
  *  When initialized the stampings page get user data and subscribe to an update event 
  **/
  ngOnInit() {
    this.getUserData();
    this.events.subscribe('update Checkin History', () => {
      this.getUserData();
    });
  }

  /**
  *  Get user informations about the checkins and checkouts. 
  **/
  getUserData() {
    this.stampingsService.getStampingsHistory().subscribe((response : UserInfo[]) => {
      
      if (response.length > 0) {
        this.userInfo = response;
        this.dataComming = true;
      }
    }, error => {
      this.error = 'Non è stato effettuato nessuna entrata o uscita!';
    });
  }

  /**
  *  Go to home page when back clicked. 
  **/
  backButton() {
    this.uiService.presentLoading('Per favore attendi...');
    this.navCtrl.navigateBack(['/home']);
    this.uiService.dismissLoading();
  }


  /**
  *  Open a popup to show information about the note. 
  **/
  openpopover(note) {
    this.presentAlert(note);
  }

  async presentAlert(note) {
    const alert = await this.alertController.create({
      header: 'Nota',
      message: note,
      buttons: [
        {
          text: 'chiudi',
          role: 'cancel',
        }
      ]
    });

    await alert.present();
  }

}

# Project moviTIMESHEET

Utilized Ionic4 & Angular8

## Installation

Install globaly ionic & cordova.
------------------------------------------------
npm install -g ionic
npm install -g cordova

Then install projects library, within the projects files.
------------------------------------------------
npm install


##Api url configuration

Path of config file : 
src/app/helpers/constant/constant.ts

parameters to be chagned:
base_url
url


Create Android/iOS platform
------------------------------------------------
ionic cordova platform add android/ios (one or the other)

##Build for specific platform

ionic cordova build android/ios --prod


##Deployment informations

Appstore: https://ionicframework.com/docs/deployment/app-store
Play Store: https://ionicframework.com/docs/deployment/play-store
